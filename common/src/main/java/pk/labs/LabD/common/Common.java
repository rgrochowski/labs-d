package pk.labs.LabD.common;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;

public class Common implements Animal {

    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private String status;
    private String name;
    private String species;
    
     private AtomicReference<Logger> logger = new AtomicReference<>();
  
  
    void bindLogger(Logger logger) {
                  System.out.println("BINDING LOGGER");
                  this.logger.set(logger);
          }
    void unbindLogger(Logger logger) {
                  this.logger.set(null);
          }
  
    @Override
    public String getSpecies() {
        return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        String oldStatus = this.status;
        this.status = status;
        this.pcs.firePropertyChange("status", oldStatus, status);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
            this.pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }
    
     public void activate(Map<String,?> properties) {
            System.out.println("Start!");
            this.name = (String)properties.get("name");
            this.species = (String)properties.get("species");
            this.status = (String)properties.get("status");
            logger.get().log(this, this.name + "START");
    }

    public void deactivate() {
         System.out.println("Stop!");
         logger.get().log(this, this.name+" STOP");
    }
}
