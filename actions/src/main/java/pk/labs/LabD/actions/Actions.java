/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabD.actions;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.AnimalAction;


public class Actions implements AnimalAction   {

    private String status;
    
    public Actions(String status) {
        this.status = status;
    }
      
    @Override
    public boolean execute(Animal animal) {
        animal.setStatus(status);
        return true;
    }

}
